﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DTAC.Task.Utils;

namespace RerunJob
{
    public partial class FrmRerun : Form
    {
        private BatchConfig batchConfig = new BatchConfig();
        public FrmRerun()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnRerun_Click(object sender, EventArgs e)
        {
            BatchProcess batch = cboBatchList.SelectedItem as BatchProcess;
            if (batch.BatchType.ToLower() == "query")
            {
                var result = MessageBox.Show("Rerun Batch " + batch, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        var path = batch.BatchParameters.Where(x => x.ParameterName.ToLower() == "batch_path").FirstOrDefault();
                        if (path != null)
                        {
                            var _dateFrom = DateTime.MinValue;
                            var _dateTo = DateTime.MinValue;
                            Process myProcess = new Process();
                            myProcess.StartInfo.FileName = path.ParameterValue;
                            var paraDateFrom = batch.BatchParameters.Where(x => x.ParameterName.ToLower() == "from").FirstOrDefault();
                            if (paraDateFrom != null)
                            {
                                if (paraDateFrom.ParameterType.ToLower() == "datetime")
                                {
                                    _dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0);
                                    myProcess.StartInfo.Arguments += _dateFrom.ToString("yyyy-MM-ddTHH:mm:ssZ ");
                                }
                                else if (paraDateFrom.ParameterType.ToLower() == "date")
                                {
                                    _dateFrom = new DateTime(dateFrom.Value.Year, dateFrom.Value.Month, dateFrom.Value.Day, 0, 0, 0);
                                    myProcess.StartInfo.Arguments += _dateFrom.ToString("yyyy-MM-ddTHH:mm:ssZ ");
                                }
                            }
                            var paraDateTo = batch.BatchParameters.Where(x => x.ParameterName.ToLower() == "to").FirstOrDefault();
                            if (paraDateTo != null)
                            {
                                if (paraDateTo.ParameterType.ToLower() == "datetime")
                                {
                                    _dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, timeTo.Value.Hour, timeTo.Value.Minute, 0);
                                    myProcess.StartInfo.Arguments += _dateTo.ToString("yyyy-MM-ddTHH:mm:ssZ ");
                                }
                                else if (paraDateTo.ParameterType.ToLower() == "date")
                                {
                                    _dateTo = new DateTime(dateTo.Value.Year, dateTo.Value.Month, dateTo.Value.Day, 0, 0, 0);
                                    myProcess.StartInfo.Arguments += _dateTo.ToString("yyyy-MM-ddTHH:mm:ssZ ");
                                }
                            }
                            myProcess.Start();
                        }
                        MessageBox.Show("Rerun Batch " + batch, "Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        initUI();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Rerun Batch " + batch + " : " + ex.Message, "Processing Fail", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    cboBatchList.SelectedItem = null;
                }
            }
        }

        private void FrmRerun_Load(object sender, EventArgs e)
        {
            this.Text += string.Format(" Version {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(3)); ;
            initUI();

            var config = System.IO.File.ReadAllText(Application.StartupPath + "\\Config.json");
            batchConfig = JsonConvert.DeserializeObject<BatchConfig>(config);
            if (batchConfig != null && batchConfig.BatchList.Count > 0)
            {
                foreach (var batch in batchConfig.BatchList)
                {
                    cboBatchList.Items.Add(batch);
                }
            }
        }

        private void cboBatchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BatchProcess batch = ((ComboBox)sender).SelectedItem as BatchProcess;
            
            initUI(batch);
        }

        private void initUI(BatchProcess batch = null)
        {
            cboDay.SelectedItem = null;

            if (batch == null)
            {
                btnRerun.Enabled = false;
                lblFTPPath.Visible = false;
                chkListFile.Visible = false;
                dateFrom.Visible = false;
                timeFrom.Visible = false;
                lblFromDate.Visible = false;
                lblFromTime.Visible = false;
                dateTo.Visible = false;
                timeTo.Visible = false;
                lblToDate.Visible = false;
                lblToTime.Visible = false;
                cboDay.Visible = false;
                lblDay.Visible = false;
                cboBatchList.SelectedItem = null;
            }
            else if(batch.BatchType.ToLower() == "sftp")
            {
                btnRerun.Enabled = false;
                lblFTPPath.Visible = true;
                chkListFile.Visible = true;
                dateFrom.Visible = false;
                timeFrom.Visible = false;
                lblFromDate.Visible = false;
                lblFromTime.Visible = false;
                dateTo.Visible = false;
                timeTo.Visible = false;
                lblToDate.Visible = false;
                lblToTime.Visible = false;
                cboDay.Visible = true;
                lblDay.Visible = true;
                cboDay.SelectedItem = DateTime.Today.ToString("dd");
            }
            else if (batch.BatchType.ToLower() == "query")
            {
                btnRerun.Enabled = true;
                lblFTPPath.Visible = false;
                chkListFile.Visible = false;
                cboDay.Visible = false;
                lblDay.Visible = false;
                foreach (var parameter in batch.BatchParameters)
                {
                    switch (parameter.ParameterName.ToLower())
                    {
                        case "from":
                            if (parameter.ParameterType.ToLower() == "datetime")
                            {
                                dateFrom.Visible = true;
                                timeFrom.Visible = true;
                                lblFromDate.Visible = true;
                                lblFromTime.Visible = true;
                            }
                            else if (parameter.ParameterType.ToLower() == "date")
                            {
                                dateFrom.Visible = true;
                                timeFrom.Visible = false;
                                lblFromDate.Visible = true;
                                lblFromTime.Visible = false;
                            }
                            break;
                        case "to":
                            if (parameter.ParameterType.ToLower() == "datetime")
                            {
                                dateTo.Visible = true;
                                timeTo.Visible = true;
                                lblToDate.Visible = true;
                                lblToTime.Visible = true;
                            }
                            else if (parameter.ParameterType.ToLower() == "date")
                            {
                                dateTo.Visible = true;
                                timeTo.Visible = false;
                                lblToDate.Visible = true;
                                lblToTime.Visible = false;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void chkListFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((CheckedListBox)sender).CheckedItems.Count != 0)
                btnRerun.Enabled = true;
            else
                btnRerun.Enabled = false;
        }

    }

    public class BatchConfig
    {
        public List<BatchProcess> BatchList { get; set; }
    }

    public class BatchProcess
    {
        public string BatchName { get; set; }
        public string BatchType { get; set; }
        public List<BatchParameter> BatchParameters { get; set; }
        public override string ToString()
        {
            return BatchName;
        }
    }

    public class BatchParameter
    {
        public string ParameterName { get; set; }
        public string ParameterType { get; set; }
        public string ParameterValue { get; set; }
    }
}
