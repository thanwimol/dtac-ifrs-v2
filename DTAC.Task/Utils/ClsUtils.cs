﻿using FluentEmail;
using NLog;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using static DTAC.Task.Utils.ClsEnum;

namespace DTAC.Task.Utils
{
    public static class ClsUtils
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public static void UploadFTPFile(string ftpServer, string ftpUser, string ftpPass, string folderName, string fileName, bool isCreateFolder = true)
        {
            FtpWebRequest request;

            string absoluteFileName = Path.GetFileName(fileName);

            if (isCreateFolder)
            {
                try
                {
                    request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}", ftpServer, folderName))) as FtpWebRequest;
                    request.Proxy = null;
                    request.EnableSsl = true;
                    request.Method = WebRequestMethods.Ftp.MakeDirectory;
                    request.Credentials = new NetworkCredential(ftpUser, ftpPass);
                    using (var resp = (FtpWebResponse)request.GetResponse())
                    {
                        //create success
                    }
                }
                catch (WebException ex)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        response.Close();
                        //already exists
                    }
                    else
                    {
                        response.Close();
                        throw new Exception(ex.Message);
                    }
                }
            }

            request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}", ftpServer, folderName, absoluteFileName))) as FtpWebRequest;
            request.Proxy = null;
            request.EnableSsl = true;
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UseBinary = true;
            request.UsePassive = true;
            request.KeepAlive = true;
            request.Credentials = new NetworkCredential(ftpUser, ftpPass);

            using (FileStream fs = File.OpenRead(fileName))
            {
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close();
            }

        }

        public static void UploadSFTPFile(string ftpServer, string ftpUser, string ftpPass, string folderName, string fileName)
        {
            using (var client = new SftpClient(ftpServer, 22, ftpUser, ftpPass))
            {
                client.Connect();

                client.ChangeDirectory(folderName);

                using (var fileStream = new FileStream(fileName, FileMode.Open))
                {
                    client.BufferSize = 4 * 1024; // bypass Payload error large files
                    client.UploadFile(fileStream, Path.GetFileName(fileName));
                }
            }            
        }

        public static void SendEmail(string batch_name, string process_detail, string smtp_host, string smtp_port, string smtp_user, string smtp_password, List<string> mail_to, MailType mailType, MailStatus mailStatus, DateTime runtime, string moreDetail)
        {
            var emailSubject = string.Empty;
            var emailBody = new StringBuilder();

            emailSubject = string.Format("Subject: Batch Summary: {0} as of {1} run {2}", batch_name, runtime.ToString("dd/MM/yyyy"), mailType.ToString());

            emailBody.AppendLine(string.Format("<p>Description: Batch {0} as of {1} run {2}</p>", batch_name, runtime.ToString("dd/MM/yyyy"), mailType.ToString()));
            emailBody.AppendLine(string.Format("<p>Batch as of: {0}</p>", runtime.ToString("dd/MM/yyyy HH:mm:ss")));
            emailBody.AppendLine(string.Format("<p>Status: {0}</p>", System.Web.HttpUtility.HtmlEncode(mailStatus.ToString())));
            emailBody.AppendLine(string.Format("<p>{0}</p>", moreDetail));
            emailBody.AppendLine(string.Format("<p>Detail: {0}</p>", System.Web.HttpUtility.HtmlEncode(process_detail)));
            emailBody.AppendLine("<p><b style='color:red;'>อีเมลล์นี้ถูกส่งโดยอัตโนมัติจากระบบ โปรดอย่าตอบกลับ</b></p>");
            emailBody.AppendLine("<p><b style='color:red;'>This is an automatically generated email. Please do not reply. </b></p>");

            WebRequest.DefaultWebProxy = new WebProxy("http:\\www-gw.tac.co.th", 8080);
            var client = new SmtpClient();
            if (smtp_host == "smtp.gmail.com")
            {
                client = new SmtpClient(smtp_host, int.Parse(smtp_port))
                {
                    Credentials = new NetworkCredential(smtp_user, smtp_password),
                    EnableSsl = true
                };
            }
            else
            {
                client = new SmtpClient(smtp_host);
            }

            var email = Email
                .From("Auto_Generate@ii.co.th", "SalesForce Administrator")
                .Subject(emailSubject)
                .Body(emailBody.ToString())
                .UsingClient(client);

            foreach (var mail in mail_to)
            {
                email.To(mail);
            }

            email.Send();
        }

        public static void TestSendMail(string batch_name, string process_detail, string smtp_host, List<string> mail_to, string status, DateTime runtime, string moreDetail)
        {
            MailMessage msg = new MailMessage();

            var emailSubject = string.Empty;
            var emailBody = new StringBuilder();

            emailSubject = string.Format("Subject: Batch Summary: {0} as of {1} run {2}", batch_name, runtime.ToString("dd/MM/yyyy"), status);

            emailBody.AppendLine("<style type=\"text/css\">");
            emailBody.AppendLine("table td{border:solid 1px;}");
            emailBody.AppendLine("</style>");
            emailBody.AppendLine(string.Format("<p>Description: Retail Batch {0} as of {1} run {2}</p>", batch_name, runtime.ToString("dd/MM/yyyy"), status));
            emailBody.AppendLine(string.Format("<p>Batch as of: {0}</p>", runtime.ToString("dd/MM/yyyy HH:mm:ss")));
            emailBody.AppendLine(string.Format("<p>Status: {0}</p>", status));
            emailBody.AppendLine(string.Format("<p>{0}</p>", moreDetail));
            emailBody.AppendLine(string.Format("<p>Detail: {0}</p>", process_detail));
            emailBody.AppendLine("<p><b style='color:red;'>อีเมลล์นี้ถูกส่งโดยอัตโนมัติจากระบบ โปรดอย่าตอบกลับ</b></p>");
            emailBody.AppendLine("<p><b style='color:red;'>This is an automatically generated email. Please do not reply. </b></p>");

            msg.From = new MailAddress("YourMail@gmail.com");
            msg.To.Add("ataponp@live.com");
            msg.Subject = emailSubject;
            msg.Body = emailBody.ToString();
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new NetworkCredential("atp@ii.co.th", "dev4fun!");
            client.Timeout = 20000;
            try
            {
                client.Send(msg);
            }
            catch (Exception)
            {
            }
            finally
            {
                msg.Dispose();
            }
        }

        public static void DoWaitAndRetry(Action action, TimeSpan retryInterval, int maxAttemptCount = 3)
        {
            var tries = 0;
            while (true)
            {
                try
                {
                    action();
                    break; // success!
                }
                catch(Exception e)
                {
                    if (tries++ == maxAttemptCount)
                        throw;
                    Thread.Sleep(retryInterval);
                }
            }
        }

        public static void GetFolderName(DateTime date, ref string folder_date, ref string day)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    folder_date = "07_SUNDAY";
                    day = "SUN";
                    break;
                case DayOfWeek.Monday:
                    folder_date = "01_MONDAY";
                    day = "MON";
                    break;
                case DayOfWeek.Tuesday:
                    folder_date = "02_TUESDAY";
                    day = "TUE";
                    break;
                case DayOfWeek.Wednesday:
                    folder_date = "03_WEDNESDAY";
                    day = "WED";
                    break;
                case DayOfWeek.Thursday:
                    folder_date = "04_THURSDAY";
                    day = "THR";
                    break;
                case DayOfWeek.Friday:
                    folder_date = "05_FRIDAY";
                    day = "FRI";
                    break;
                case DayOfWeek.Saturday:
                    folder_date = "06_SATURDAY";
                    day = "SAT";
                    break;
            }
        }

    }
}
