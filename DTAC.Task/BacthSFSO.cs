﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Salesforce.Common;
using Salesforce.Force;
using DTAC.Task.Utils;
using static DTAC.Task.Utils.ClsEnum;

namespace DTAC.Task
{
    public class BatchSFSO
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public AuthenticationClient auth = new AuthenticationClient();
        private Dictionary<string, string> _config = new Dictionary<string, string>();
        private DateTime batch_Current_Runtime_UTC7;
        private string batch_FileName_DAT = "{0}_SALESFORCE_SELL_OUT.DAT";
        private string batch_FileName_SYNC = "{0}_SALESFORCE_SELL_OUT.SYNC";
        private string folder_date = string.Empty;
        private string folder_day = string.Empty;
        private string job_id = string.Empty;
        private int max_retry = 3;
        private int retry_timeout = 30;
        private string SFTP_IP = string.Empty;
        private string SFTP_User = string.Empty;
        private string SFTP_Password = string.Empty;
        private string SMTP_Host = string.Empty;
        private string SMTP_Port = string.Empty;
        private string SMTP_User = string.Empty;
        private string SMTP_Password = string.Empty;
        private List<string> EmailNotify = new List<string>();
        private string strAfterRetry = string.Empty;
        private string OutputFolder = string.Empty;

        private const string pro_sf = "Get Data From Saleforce";
        private const string pro_sf_detail_str = "Start Get Data From Saleforce";
        private const string pro_sf_detail_fin = "Finish Get Data From Saleforce";
        private const string pro_sf_detail_err = "Error Get Data From Saleforce <{0}>";

        private const string pro_upload = "SFTP Upload";
        private const string pro_upload_detail_str = "Start SFTP upload <{0}> to <{1}>";
        private const string pro_upload_detail_fin = "Finish SFTP upload <{0}> to <{1}>";
        private const string pro_upload_detail_err = "Error SFTP upload <{0}> in <{1}> <{2}>";

        private const string pro_complete = "Email notification";
        private const string pro_complete_detail = "Complete Get Data From Saleforce batch, {0} Total {1} Records";
        
        private int recordsCount = 0;

        public BatchSFSO(Dictionary<string, string> config, List<string> emailNotify)
        {
            _config = config;
            EmailNotify = emailNotify;
            if (!string.IsNullOrEmpty(_config["Retry_Count"]))
            {
                max_retry = int.Parse(_config["Retry_Count"]);
            }
            if (!string.IsNullOrEmpty(_config["Retry_Timeout"]))
            {
                retry_timeout = int.Parse(_config["Retry_Timeout"]);
            }

            strAfterRetry = string.Format(" after <{0}> attempts", max_retry.ToString());
            SFTP_IP = _config["SFTP_IP"];
            SFTP_User = _config["SFTP_User"];
            SFTP_Password = _config["SFTP_Password"];
            OutputFolder = _config["SFTP_OutputFolder"];
            SMTP_Host = _config["SMTP_Host"];
            SMTP_Port = _config["SMTP_Port"];
            SMTP_User = _config["SMTP_User"];
            SMTP_Password = _config["SMTP_Password"];
        }

        public void StartProcess(DateTime current_runtimeUTC7, DateTime startFrom, DateTime endTo)
        {
            batch_Current_Runtime_UTC7 = current_runtimeUTC7;

            ClsUtils.GetFolderName(batch_Current_Runtime_UTC7, ref folder_date, ref folder_day);

            batch_FileName_DAT = string.Format(batch_FileName_DAT, (batch_Current_Runtime_UTC7).AddDays(-1).ToString("yyyyMMdd"));
            batch_FileName_SYNC = string.Format(batch_FileName_SYNC, batch_Current_Runtime_UTC7.AddDays(-1).ToString("yyyyMMdd"));

            if (startFrom == DateTime.MinValue)
            {
                startFrom = new DateTime(DateTime.UtcNow.AddHours(7).Year, DateTime.UtcNow.AddHours(7).Month, DateTime.UtcNow.AddHours(7).Day);
                startFrom = startFrom.AddDays(-1);
                endTo = new DateTime(DateTime.UtcNow.AddHours(7).Year, DateTime.UtcNow.AddHours(7).Month, DateTime.UtcNow.AddHours(7).Day);
            }
            //else
            //{
            //    batch_FileName_DAT = batch_FileName_DAT.Replace(batch_Current_Runtime_UTC7.ToString("yyyyMMdd_"),"");
            //    batch_FileName_SYNC = batch_FileName_SYNC.Replace(batch_Current_Runtime_UTC7.ToString("yyyyMMdd_"), "");
            //}

            List<dynamic> batchQueryResults = new List<dynamic>();

            #region SFDC

            try
            {
                ClsUtils.DoWaitAndRetry(
                    () =>
                    {
                        string[] readQuery = null;
                        var _query = new StringBuilder();
                        readQuery = File.ReadAllLines(@"BatchQuerySubmitLineItem.txt");

                        //readQuery = File.ReadAllLines(@"BatchQuery.txt"); 
                        //readQuery = File.ReadAllLines(@"BatchQueryMigration.txt");
                        if (readQuery.Length == 0)
                            throw new Exception();
                        foreach (var item in readQuery)
                        {
                            var _lineQuery = item.Replace("[start]", startFrom.AddHours(-7).ToString("yyyy-MM-ddTHH:mm:ssZ"));
                            _lineQuery = _lineQuery.Replace("[end]", endTo.AddHours(-7).ToString("yyyy-MM-ddTHH:mm:ssZ"));
                            _query.AppendLine(_lineQuery);
                        }

                        var data = GetDataByQuery(_query.ToString()).Result;

                        recordsCount = data.Count;

                        SaveDataToFile(data);
                        //SaveMigrationDataToFile(data);
                    },
                    TimeSpan.FromSeconds(retry_timeout),
                    max_retry);

            }
            catch (Exception ex)
            {
                ErrorProcess(pro_sf, string.Format(pro_sf_detail_err, ex.Message), ex.Message);

                return;//Finish Job
            }

            #endregion
            
            #region SFTP

            try
            {
                var IsNormalFTP = _config["FTP_Type"].ToLower() == "ftp" ? true : false;

                ClsUtils.DoWaitAndRetry(
                    () =>
                    {
                        if (IsNormalFTP)
                        {
                            //// Dev Sandbox
                            ClsUtils.UploadFTPFile(SFTP_IP, SFTP_User, SFTP_Password, OutputFolder, AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_DAT);
                            ClsUtils.UploadFTPFile(SFTP_IP, SFTP_User, SFTP_Password, OutputFolder, AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_SYNC);

                        }
                        else
                        {
                            // Production only
                            ClsUtils.UploadSFTPFile(SFTP_IP, SFTP_User, SFTP_Password, OutputFolder, AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_DAT);
                            ClsUtils.UploadSFTPFile(SFTP_IP, SFTP_User, SFTP_Password, OutputFolder, AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_SYNC);

                        }
                    },
                    TimeSpan.FromSeconds(retry_timeout),
                    max_retry);

            }
            catch (Exception ex)
            {
                ErrorProcess(pro_upload, string.Format(pro_upload_detail_err, batch_FileName_DAT, OutputFolder, ex.Message), ex.Message);

                return;//Finish Job
            }

            #endregion

            #region Email Complete

//            SendMail(string.Format(pro_complete_detail, batch_FileName_DAT, recordsCount.ToString()), "Pass", MailType.successfully, MailStatus.Success);

            #endregion
        }

        private void SaveDataToFile(List<dynamic> batchQueryResults)
        {

            var outputResult = new StringBuilder();

            var sumAmount = 0.0;
            List<dynamic> selectedResults = new List<dynamic>();
            foreach (var item in batchQueryResults)
            {

                // [09-May-2018] If CustomerNumber == Empty, reject that row
                string CustomerNumber = GetCustomerNumber_SubLineItem(item);
                if(CustomerNumber == null || CustomerNumber == string.Empty)
                {
                    continue;
                }
                else
                {
                    // [15-May-2018] for summary number
                    selectedResults.Add(item);
                }

                double amount = GetAmount_SubLineItem(item);
                sumAmount += amount;

                string model = GetData(item.Device_Model_T__c);
                if (model.Length > 60)
                {
                    model = model.Substring(0, 60);
                }

                outputResult.Append(GetCompanyCode_SubLineItem(item) + "|");
                //outputResult.Append(GetCustomerNumber_SubLineItem(item) + "|");
                outputResult.Append(CustomerNumber + "|");
                outputResult.Append(GetData(item.Subscriber_No__c) + "|");
                outputResult.Append(GetPackageCode_SubLineItem(item) + "|");
                outputResult.Append(GetStrDateData(item.LastModifiedDate) + "|");
                outputResult.Append(GetContractPeriod_SubLineItem(item) + "|");
                outputResult.Append(GetData(item.Handset_Product_Code__c) + "|");
                outputResult.Append(model + "|");
                outputResult.Append(amount.ToString("0.00") + "|");
                outputResult.Append("|");
                outputResult.Append(GetData(item.Id) +"\n");


                //outputResult.Append(GetCompanyCode(item) + "|");
                //outputResult.Append(GetData(item.CC_B_Customer_Code__c) + "|");
                //outputResult.Append(GetData(item.Subscriber_Number__c) + "|");
                //outputResult.Append(GetData(item.First_Package_Group_Code__c) + "|");
                //outputResult.Append(GetStrDateData(item.Last_Modified_Date_Time__c) + "|");
                //outputResult.Append(GetPeriod(item.Submit_Line_Item__r) + "|");
                //outputResult.Append(GetData(item.Submit_Line_Item__r.Handset_Product_Code__c) + "|");
                //outputResult.Append(model + "|");
                //outputResult.Append(amount.ToString("0.00") + "|");
                //outputResult.Append("|");
                //outputResult.Append(GetData(item.Id) +"\n");
            }

            WriteOutput(AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_DAT, outputResult.ToString());
            WriteOutput(AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_SYNC, selectedResults.Count.ToString() + "|" + sumAmount.ToString("0.00"));
        }

        private void SaveMigrationDataToFile(List<dynamic> batchQueryResults)
        {

            var outputResult = new StringBuilder();

            #region Header

            #endregion
            var sumAmount = 0.0;
            var sumCount = 0;
            foreach (var item in batchQueryResults)
            {
                var srtPreriod = GetPeriod(item.Submit_Line_Item__r);
                DateTime modifiedDate = item.Last_Modified_Date_Time__c.Value;
                if (srtPreriod != "Others" && modifiedDate.AddMonths(int.Parse(srtPreriod)) > new DateTime(2017, 12, 31))
                //if (srtPreriod == "Others")
                {
                    sumCount++;
                    double amount = GetAmount(item.Submit_Line_Item__r);
                    sumAmount += amount;
                    string model = GetData(item.Submit_Line_Item__r.Device_Model_T__c);
                    if (model.Length > 60)
                        model = model.Substring(0, 60);
                    outputResult.Append(GetCompanyCode(item.Submit_Line_Item__r) + "|");
                    outputResult.Append(GetData(item.CC_B_Customer_Code__c) + "|");
                    outputResult.Append(GetData(item.Subscriber_Number__c) + "|");
                    outputResult.Append(GetData(item.First_Package_Group_Code__c) + "|");
                    outputResult.Append(GetStrDateData(item.Last_Modified_Date_Time__c) + "|");
                    outputResult.Append(srtPreriod + "|");
                    outputResult.Append(GetData(item.Submit_Line_Item__r.Handset_Product_Code__c) + "|");
                    outputResult.Append(model + "|");
                    outputResult.Append(amount.ToString("0.00") + "|");
                    outputResult.Append("|");
                    outputResult.Append(GetData(item.Id) + "\n");
                }
            }

            WriteOutput(AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_DAT, outputResult.ToString());
            WriteOutput(AppDomain.CurrentDomain.BaseDirectory + "Data\\" + batch_FileName_SYNC, sumCount.ToString() + "|" + sumAmount.ToString("0.00"));
        }

        private void WriteOutput(string filepath, string data)
        {
            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Data\\");
            File.WriteAllText(filepath, data, Encoding.UTF8);
            //File.WriteAllText(filepath, data, Encoding.ASCII);
        }

        private void ErrorProcess(string process, string process_detail, string message_input = "")
        {
            SendMail(process_detail + strAfterRetry, "Fail", MailType.failed, MailStatus.Failed);
        }

        private void SendMail(string process_detail, string status, MailType mailType, MailStatus mailStatus)
        {
            try
            {
                _logger.Info(process_detail);

                ClsUtils.SendEmail(batch_FileName_DAT, process_detail, SMTP_Host, SMTP_Port, SMTP_User, SMTP_Password, EmailNotify, mailType, mailStatus, batch_Current_Runtime_UTC7, "");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }



        /*------------  SubLineItem  --------------*/

        private double GetAmount_SubLineItem(dynamic sli_data)
        {
            var result = 0.0;
            if (sli_data.Sub_Agreement__r != null)
            {
                // GetAmount of Sub_Agreement
                var amountStr = sli_data.Sub_Agreement__r.Market_Price__c.ToString();
                if (!string.IsNullOrEmpty(amountStr))
                    result = double.Parse(amountStr);
            }
            else
            {
                // GetAmount of Agreement
                var amountStr = sli_data.Agreement__r.Market_Price__c.ToString();
                if (!string.IsNullOrEmpty(amountStr))
                    result = double.Parse(amountStr);
            }
            return result;
        }

        private string GetContractPeriod_SubLineItem(dynamic sli_data)
        {
            string result = null;
            if (sli_data.Sub_Agreement__r != null)
            {
                // GetAmount of Sub_Agreement
                var subagreement_cp = sli_data.Sub_Agreement__r.Contract_Period__c;
                result = subagreement_cp;
            }
            else
            {
                // GetAmount of Agreement
                var agreement_cp = sli_data.Agreement__r.Contract_Period__c;
                result = agreement_cp;
            }
            return result;
        }

        private string GetCompanyCode_SubLineItem(dynamic sli_data)
        {
            var result = string.Empty;
            result = sli_data.Agreement__r == null ?
                (sli_data.Sub_Agreement__r.Company_Header__c == null ? string.Empty: sli_data.Sub_Agreement__r.Company_Header__c) 
                : sli_data.Agreement__r.Company_Header__r.Name.ToString();
            if (result.ToLower() == "dtac trinet")
                result = "dtn";
            else
                result = result.ToLower();
            return result;
        }

        private string GetPackageCode_SubLineItem(dynamic sli_data)
        {
            var result = string.Empty;

            if(sli_data.Agreement__r != null)
            {
                // get Agreement's main package
                result = sli_data.Agreement__r.Main_Package__r.Package_Code__c;
            }
            else
            {
                // get SubAgreement's main package
                result = sli_data.Sub_Agreement__r.Main_Package__r.Package_Code__c;
            }

            return result;
        }

        private string GetCustomerNumber_SubLineItem(dynamic sli_data)
        {
            var result = string.Empty;
            if (sli_data.Agreement__r != null)
            {
                // get Agreement's CCB_Cust_Number
                // result = sli_data.Agreement__r.CC_B_Account__r == null ? string.Empty : sli_data.Agreement__r.CC_B_Account__r.CCB_Cust_Number__c;
                if(sli_data.Agreement__r.CC_B_Account__r == null)
                {
                    result = GetCustomerNumber_CaseNullAgreement(GetData(sli_data.Id));
                }
                else
                {
                    result = sli_data.Agreement__r.CC_B_Account__r.CCB_Cust_Number__c;
                }
            }
            else
            {
                // get SubAgreement's CCB_Cust_Number
                // result = sli_data.Sub_Agreement__r.CC_B_Account__r == null ? string.Empty : sli_data.Sub_Agreement__r.CC_B_Account__r.CCB_Cust_Number__c;
                if (sli_data.Sub_Agreement__r.CC_B_Account__r == null)
                {
                    result = GetCustomerNumber_CaseNullAgreement(GetData(sli_data.Id));
                }
                else
                {
                    result = sli_data.Sub_Agreement__r.CC_B_Account__r.CCB_Cust_Number__c;
                }
            }
            return result;
        }

        private string GetCustomerNumber_CaseNullAgreement(string Submit_Line_ItemID)
        {
            try
            {
                List<dynamic> RegIntJobLine = new List<dynamic>();
                string CCBCustomerCode = "";
                ClsUtils.DoWaitAndRetry(
                    () =>
                    {
                        string[] readQuery = null;
                        var _query = new StringBuilder();
                        readQuery = File.ReadAllLines(@"BatchQueryRegIntegJobLine.txt"); // The secode query  string

                        if (readQuery.Length == 0)
                        {
                            throw new Exception();
                        }
                           
                        foreach (var item in readQuery)
                        {
                            var _lineQuery = item.Replace("[SLIID]", "'"+ Submit_Line_ItemID + "'");
                            _query.AppendLine(_lineQuery);
                        }

                        RegIntJobLine = GetDataByQuery(_query.ToString()).Result;
                        if(RegIntJobLine.Count > 0)
                        {
                            foreach (var item in RegIntJobLine)
                            {
                                CCBCustomerCode = item.CC_B_Customer_Code__c;
                            }
                        } 
                    },
                    TimeSpan.FromSeconds(retry_timeout),
                    max_retry);

                return CCBCustomerCode;

            }
            catch (Exception ex)
            {
                ErrorProcess(pro_sf, string.Format(pro_sf_detail_err, ex.Message), ex.Message);

                return "";//Finish Job
            }


        }

        /*--------------------------*/




        private string GetCompanyCode(dynamic data)
        {
            var result = string.Empty;
            result = data == null ? string.Empty : data.Submit_Register__r.Agreement__r.Company_Header__r.Name.ToString();
            if (result.ToLower() == "dtac trinet")
                result = "dtn";
            else
                result = result.ToLower();
            return result;
        }

        private double GetAmount(dynamic data)
        {
            var result = 0.0;
            if (data.Submit_Register__r.Sub_Agreement__r == null)
            {
                var amountStr = data.Submit_Register__r.Agreement__r.Market_Price__c.ToString();
                if (!string.IsNullOrEmpty(amountStr))
                    result = double.Parse(amountStr);
            }
            else
            {
                var amountStr = data.Submit_Register__r.Sub_Agreement__r.Market_Price__c.ToString();
                if (!string.IsNullOrEmpty(amountStr))
                    result = double.Parse(amountStr);
            }
            return result;
        }

        private string GetPeriod(dynamic data)
        {
            var result = string.Empty;
            if (data.Submit_Register__r.Sub_Agreement__r == null)
            {
                result = data.Submit_Register__r.Agreement__r.Contract_Period__c.ToString();
            }
            else
            {
                result = data.Submit_Register__r.Sub_Agreement__r.Contract_Period__c.ToString();
            }
            return result;
        }

        private string GetData(dynamic data)
        {
            return data == null ? string.Empty : data.ToString().Trim();
        }

        private string GetStrDateUTC7Data(dynamic data)
        {
            if (data != null)
            {
                var dateData = DateTime.MinValue;
                dateData = data.Value;
                if (dateData != DateTime.MinValue)
                    return dateData.AddHours(7).ToString("yyyy-MM-dd HH:mm:ss");
                else
                    return string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        private string GetStrDateData(dynamic data)
        {
            if (data != null)
            {
                var dateData = DateTime.MinValue;
                dateData = data.Value;
                if (dateData != DateTime.MinValue)
                    return dateData.ToString("yyyy-MM-dd HH:mm:ss");
                else
                    return string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }




        public async System.Threading.Tasks.Task GetToken()
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var IsSandboxUser = _config["SF_IsSandboxUser"].ToLower() == "true" ? true : false;
                var url = IsSandboxUser ? "https://test.salesforce.com/services/oauth2/token" : "https://login.salesforce.com/services/oauth2/token";
                await auth.UsernamePasswordAsync(_config["SF_ConsumerKey"], _config["SF_ConsumerSecret"], _config["SF_Username"], _config["SF_Password"] + _config["SF_SecurityToken"], url);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
          
        }

        public async System.Threading.Tasks.Task<List<dynamic>> GetDataByQuery(string query)
        {
            await GetToken();

            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

            var data = new List<dynamic>();
            var results = await client.QueryAsync<dynamic>(query);

            data.AddRange(results.Records);
            var nextRecordsUrl = results.NextRecordsUrl;

            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    var continuationResults = await client.QueryContinuationAsync<dynamic>(nextRecordsUrl);

                    data.AddRange(continuationResults.Records);
                    if (string.IsNullOrEmpty(continuationResults.NextRecordsUrl)) break;

                    //pass nextRecordsUrl back to client.QueryAsync to request next set of records
                    nextRecordsUrl = continuationResults.NextRecordsUrl;
                }
            }

            return data;
        }
    }
}
