﻿using NLog;
using NLog.Config;
using NLog.Targets;
using Quartz;
using Quartz.Impl;
using System;
using System.ServiceProcess;

namespace BatchIntSFSO
{
    public class BatchService : ServiceBase
    {
        private static IScheduler _scheduler;

        public static void Main(string[] args)
        {
            Console.Title = string.Format("Batch IFRS Version {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(3));
            // For developer change query here
            //args = new string[] { "2018-03-19T00:00:00Z", "2018-03-19T23:59:59Z" };

            foreach (var item in args)
            {
                if (DateTime.UtcNow.Hour == DateTime.Now.Hour)
                    Console.Title += " : [" + DateTime.Parse(item).ToString("yyyy-MM-dd HH:mm:ss]");
                else
                    Console.Title += " : [" + DateTime.Parse(item).AddHours(-7).ToString("yyyy-MM-dd HH:mm:ss]");
            }

            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            // Step 2. Create targets and add them to the configuration 
            var consoleTarget = new ColoredConsoleTarget();
            config.AddTarget("console", consoleTarget);

            var fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);

            // Step 3. Set target properties 
            consoleTarget.Layout = @"${date:format=yyyy-MM-dd HH\:mm\:ss} ${logger} ${message}";
            fileTarget.FileName = @"${basedir}/log/${date:format=yyyyMMdd}.txt";
            fileTarget.Layout = @"${date:format=HH\:mm\:ss} ${logger} ${message}";

            // Step 4. Define rules
            var rule1 = new LoggingRule("*", LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(rule1);

            var rule2 = new LoggingRule("*", LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule2);

            // Step 5. Activate the configuration
            LogManager.Configuration = config;

            //// Example usage
            //Logger logger = LogManager.GetLogger("Example");
            //logger.Trace("trace log message");
            //logger.Debug("debug log message");
            //logger.Info("info log message");
            //logger.Warn("warn log message");
            //logger.Error("error log message");
            //logger.Fatal("fatal log message");

            using (var service = new BatchService())
            {
                if (Environment.UserInteractive)
                {
                    Console.CancelKeyPress += (sender, e) =>
                    {
                        service.OnStop();
                    };

                    service.OnStart(args);

                    var keyInfo = Console.ReadKey();

                    while (!(keyInfo.Key == ConsoleKey.X && keyInfo.Modifiers == ConsoleModifiers.Control) ||
                        !(keyInfo.Key == ConsoleKey.Z && keyInfo.Modifiers == ConsoleModifiers.Control))
                    {
                        keyInfo = Console.ReadKey();
                    }

                    service.OnStop();

                    return;
                }

                Run(service);
            }
        }

        protected override void OnStart(string[] args)
        {
            var schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler();

            var scheduleJob = new JobSchedule();
            scheduleJob.Initialize(_scheduler, args);

            if (!_scheduler.IsStarted)
            {
                _scheduler.Start();
            }
        }

        protected override void OnStop()
        {
            if (_scheduler != null && _scheduler.IsStarted)
            {
                _scheduler.Shutdown(true);
            }
        }

    }
}
