﻿using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace BatchIntSFSO
{
    public class JobSchedule : IJob
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private static Dictionary<string, string> config = new Dictionary<string, string>();
        private static List<string> emailNoti = new List<string>();
        private static DateTime start = DateTime.MinValue;
        private static DateTime end = DateTime.MinValue;

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var runtimeUTC7 = DateTime.UtcNow.AddHours(7);
                //var runtimeUTC7 = DateTime.UtcNow.AddHours(0);
                switch (context.Trigger.Key.Name)
                {
                    case "BatchTrigger":
                        try
                        {
                            var now = runtimeUTC7.Hour + (runtimeUTC7.Minute * 0.01);
                            
                            _logger.Info("Start.");
                            var batch = new DTAC.Task.BatchSFSO(config, emailNoti);
                            batch.StartProcess(runtimeUTC7, start, end);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(string.Format("Error. <{0}>", ex.Message));
                        }
                        finally
                        {
                            _logger.Info("Finish.");
                        }
                        break;
                    case "BatchRerunTrigger":
                        try
                        {
                            var now = runtimeUTC7.Hour + (runtimeUTC7.Minute * 0.01);
                            _logger.Info("Start.");
                            var batch = new DTAC.Task.BatchSFSO(config, emailNoti);
                            batch.StartProcess(runtimeUTC7, start, end);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(string.Format("Error. <{0}>", ex.Message));
                        }
                        finally
                        {
                             _logger.Info("Finish.");

                            Environment.Exit(0);
                        }
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                _logger.Fatal(ex.Message);
            }
        }

        public void Initialize(IScheduler scheduler, string[] args)
        {
            try
            {
                _logger.Info("Loading config.");

                config.Add("SFTP_IP", ConfigurationManager.AppSettings["SFTP_IP"].ToString());
                config.Add("SFTP_User", ConfigurationManager.AppSettings["SFTP_User"].ToString());
                config.Add("SFTP_Password", ConfigurationManager.AppSettings["SFTP_Password"].ToString());
                config.Add("SFTP_OutputFolder", ConfigurationManager.AppSettings["SFTP_OutputFolder"].ToString());

                config.Add("SMTP_Host", ConfigurationManager.AppSettings["SMTP_Host"].ToString());
                config.Add("SMTP_Port", ConfigurationManager.AppSettings["SMTP_Port"].ToString());
                config.Add("SMTP_User", ConfigurationManager.AppSettings["SMTP_User"].ToString());
                config.Add("SMTP_Password", ConfigurationManager.AppSettings["SMTP_Password"].ToString());

                config.Add("Notify_Email", ConfigurationManager.AppSettings["Notify_Email"].ToString());
                
                config.Add("SF_Username", ConfigurationManager.AppSettings["SF_Username"].ToString());
                config.Add("SF_Password", ConfigurationManager.AppSettings["SF_Password"].ToString());
                config.Add("SF_SecurityToken", ConfigurationManager.AppSettings["SF_SecurityToken"].ToString());
                config.Add("SF_ConsumerKey", ConfigurationManager.AppSettings["SF_ConsumerKey"].ToString());
                config.Add("SF_ConsumerSecret", ConfigurationManager.AppSettings["SF_ConsumerSecret"].ToString());
                config.Add("SF_IsSandboxUser", ConfigurationManager.AppSettings["SF_IsSandboxUser"].ToString());


                config.Add("Interval", ConfigurationManager.AppSettings["Interval"].ToString());
                config.Add("Retry_Count", ConfigurationManager.AppSettings["Retry_Count"].ToString());
                config.Add("Retry_Timeout", ConfigurationManager.AppSettings["Retry_Timeout"].ToString());

                config.Add("FTP_Type", ConfigurationManager.AppSettings["FTP_Type"].ToString());

                _logger.Info("Loaded config.");

                _logger.Info("Loading email.");

                var emailList = config["Notify_Email"].Split(',');

                foreach (var email in emailList)
                {
                    emailNoti.Add(email.Trim());
                }

                foreach (var email in emailNoti)
                {
                    _logger.Info(email);
                }

                _logger.Info("Loaded email.");

                if (args.Length == 2)
                {
                    start = DateTime.Parse(args[0]).AddHours(-7);
                    end = DateTime.Parse(args[1]).AddHours(-7);
                    var jobDetail = JobBuilder.Create<JobSchedule>()
                            .WithIdentity("DTAC_SFDC_Job")
                            .Build();

                    var BatchRerunTrigger = TriggerBuilder.Create()
                       .WithIdentity("BatchRerunTrigger", "ScheduleJob")
                       .StartNow()
                       .Build();


                    var triggers = new Quartz.Collection.HashSet<ITrigger>(
                        new[]
                        {
                        BatchRerunTrigger,
                        });

                    scheduler.ScheduleJob(jobDetail, triggers, true);
                }
                else
                {
                    var jobDetail = JobBuilder.Create<JobSchedule>()
                            .WithIdentity("DTAC_SFDC_Job")
                            .Build();

                    var BatchTrigger = TriggerBuilder.Create()
                       .WithIdentity("BatchTrigger", "ScheduleJob")
                       //.WithCronSchedule("0 0/" + config["Interval"] + " * * * ?")
                       .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(int.Parse(config["Interval"]), 0))
                       .StartNow()
                       .Build();


                    var triggers = new Quartz.Collection.HashSet<ITrigger>(
                        new[]
                        {
                        BatchTrigger,
                        });

                    scheduler.ScheduleJob(jobDetail, triggers, true);
                }

                _logger.Info("Has been initialized.");

            }
            catch (Exception ex)
            {
                _logger.Fatal(ex.Message);
            }
        }
    }
}
